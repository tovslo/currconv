docker-compose -f redis.yml up &
celery -A currconv beat --loglevel=debug  -l info --logfile=celery.log \
            --scheduler django_celery_beat.schedulers:DatabaseScheduler &
celery -A currconv worker --loglevel=debug --concurrency=1 &
python ./manage.py runserver

