from django.db import models


class Rate(models.Model):
    code = models.CharField(max_length=3, primary_key=True, verbose_name='Currency code')
    name = models.CharField(max_length=255, null=True, verbose_name='Currency name')
    rate = models.FloatField(null=True, verbose_name='Rate to base currency')

    def __str__(self):
        return self.code
