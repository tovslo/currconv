import pytest
from rebar.testing import flatten_to_dict

from converter.forms import CurrencyForm


@pytest.mark.parametrize('source_value, source_code, currency_code, reason',
        [
            ('', 'AAA', 'BBB', 'undefined source value'),     # undefined source value
            ('-1', 'AAA', 'BBB', 'negative source value'),   # negative source value
            ('1', '', 'BBB', 'undefined source code'),       # undefined source code
            ('1', 'AAA', '', 'undefined target code')        # undefined target code
        ])
def test_invalid_form_with_invalid_inputs(source_value, source_code, currency_code, reason):
    """ form is invalid if one of these inputs is invalid """
    form_data = flatten_to_dict(CurrencyForm())
    form_data['source_currency_value'] = source_value
    form_data['source_currency_code'] = source_code
    form_data['target_currency_code'] = currency_code

    bound_form = CurrencyForm(data=form_data)
    assert not bound_form.is_valid(), reason


@pytest.mark.parametrize('source_value, source_code, currency_code',
        [
            ('0', 'AAA', 'BBB'),
            ('100', 'AAA', 'BBB'),
            ('100.5', 'AAA', 'BBB'),
            ('1.38e5', 'AAA', 'BBB'),
        ])
def test_valid_form_with_valid_inputs(source_value, source_code, currency_code):
    """ form is invalid if one of these inputs is invalid """
    form_data = flatten_to_dict(CurrencyForm())
    form_data['source_currency_value'] = source_value
    form_data['source_currency_code'] = source_code
    form_data['target_currency_code'] = currency_code

    bound_form = CurrencyForm(data=form_data)
    assert bound_form.is_valid()

