import pytest
import json

from django.conf import settings

from converter.tasks import update_rates
from converter.models import Rate


@pytest.fixture()
def fake_currency_names():
    """ Fixture that returns a static currency_names """
    with open(settings.BASE_DIR / "converter/tests/currency_names.json") as f:
        return json.load(f)


@pytest.fixture()
def fake_currency_rates():
    """ Fixture that returns a static currency_rates """
    with open(settings.BASE_DIR / "converter/tests/currency_rates.json") as f:
        return json.load(f)


@pytest.mark.django_db
def test_update_rates_using_mocks(mocker, fake_currency_names, fake_currency_rates):
    """ Test that update currency rates in DB is working correctly without
    making requests for external API """

    mocker.patch("converter.tasks.get_currency_names", return_value=fake_currency_names)
    mocker.patch("converter.tasks.get_currency_rates", return_value=fake_currency_rates)

    rates_count = Rate.objects.count()
    assert rates_count == 0

    update_rates()

    rates_count = Rate.objects. \
        filter(rate__isnull=False). \
        filter(name__isnull=False).count()
    assert rates_count == 169



