import pytest
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_converter_view():
    client = Client()

    response = client.get(reverse('convert-currency'))
    assert response.status_code == 200

    response = client.post(reverse('convert-currency'))
    assert response.status_code == 200


# ToDo: detail view tests
