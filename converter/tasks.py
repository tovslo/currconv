from __future__ import absolute_import, unicode_literals
from celery import shared_task

import requests
from converter.models import Rate


def get_currency_names():
    return requests.get("https://openexchangerates.org/api/currencies.json").json()


def get_currency_rates():
    return requests.get("https://openexchangerates.org/api/latest.json?app_id=444b7fb25d48494e8c0d79801339e699").json()


@shared_task(name="update_rates_task")
def update_rates():
    names = get_currency_names()
    for code, name in names.items():
        rate = Rate(code=code, name=name)
        rate.save()
    rates = get_currency_rates()['rates']
    for code, value in rates.items():
        rate = Rate.objects.get(code=code)
        rate.rate = value
        rate.save()
