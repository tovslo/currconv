from django import forms

# dummy choices
choices = INTEGER_CHOICES = [('', '')]


class CurrencyForm(forms.Form):
    source_currency_value = forms.DecimalField(label='Amount', min_value=0)
    source_currency_code = forms.CharField(label='From', widget=forms.Select(choices=INTEGER_CHOICES))
    target_currency_code = forms.CharField(label='To', widget=forms.Select(choices=INTEGER_CHOICES))

    def __init__(self, tuple_country_code=None, *args, **kwargs):
        self.tuple_country_code = tuple_country_code or ()
        super(CurrencyForm, self).__init__(*args, **kwargs)

        self.fields['source_currency_code'].widget.choices = self.tuple_country_code
        self.fields['target_currency_code'].widget.choices = self.tuple_country_code