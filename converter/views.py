from django.shortcuts import render

from converter import forms, models, tasks


def converter_view(request):
    """
    Converts currency from source to target
    Uses rates to base currency, stored in DB
    """

    currency_rates = models.Rate.objects.\
        filter(rate__isnull=False).\
        filter(name__isnull=False).\
        order_by('name')

    currency_codes = [tuple([x.code, x.name]) for x in currency_rates]
    currency_form = forms.CurrencyForm(currency_codes, request.POST or None)
    converted_currency = ""

    if request.method == "POST":
        if currency_form.is_valid():
            source_code = currency_form.cleaned_data['source_currency_code']
            target_code = currency_form.cleaned_data['target_currency_code']
            source_value = currency_form.cleaned_data['source_currency_value']

            from_country_base_value = models.Rate.objects.get(code=source_code).rate
            to_country_base_value = models.Rate.objects.get(code=target_code).rate

            converted_currency = (to_country_base_value / from_country_base_value) * float(source_value)

            return render(request, 'converter/converter.html',
                          {'currency_form': currency_form, 'converted_currency': converted_currency})

    # form initialization
    context = {
        'currency_form': currency_form,
        'converted_currency': converted_currency
    }
    return render(request, 'converter/converter.html', context)
