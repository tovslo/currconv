from django.urls import path

from converter import views

urlpatterns = [
    path('convert/', views.converter_view, name='convert-currency'),
]