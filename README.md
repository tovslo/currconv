# currconv
Currency converter project. Based on Django v3.2.11, Celery v5.2.3 and Redis v6.2.6 
(for periodic tasks). Uses https://openexchangerates.org/api/ to acquire currency names and rates.
# To run project locally:
1. Clone the project.
2. Create python virtual environment.
3. Install requirements:
   ```$ pip install -r requirements.txt```
4. Apply migrations and create superuser for admin panel:
  ```$ init_db```
5. Run django server, redis server and celery:
  ```$ run_all```
6. In admin panel create periodic task for ```update_rates_task``` task and run it manually 
   for the first time to create currency rates models.
7. Open ```http://localhost:8000/currency/convert/```
# To run tests:
Run ```pytest```. All tests and test coverage are configured in ```setup.cfg``` file.
  
